/*-----------------------------------------------------------------------------
 * @package:    rngdng
 * @author:     Richard B Winters
 * @copyright:  2015-2018 Massively Modified, Inc.
 * @license:    Apache-2.0
 * @version:    0.1.0
 *---------------------------------------------------------------------------*/


 export class Task
 {
    id: number
    title: string
    description: string
 }