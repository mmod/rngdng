import { Component, OnInit } from '@angular/core';
import { Task } from '../task';

@Component({
  selector: 'app-tasks',
  templateUrl: './tasks.component.html',
  styleUrls: ['./tasks.component.css']
})

export class TasksComponent implements OnInit
{
  task: Task =
  {
    id: 1,
    title: "Build a Kanban",
    description: "Create a Kanban board using Angular 6"
  }

  constructor() { }

  ngOnInit()
  {
  }

}
